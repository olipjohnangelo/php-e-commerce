<style>
  .navbar a:hover {
  border-bottom: 3px solid yellow;
}

</style>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary border border-dark">
  <a class="navbar-brand font-weight-bold" href="../views/login.php">Accessories</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link font-weight-bold" href="../views/catalog.php">Catalog <span class="sr-only">(current)</span></a>
      </li>


      <?php 
        session_start();
        if(isset($_SESSION['name'])){
          if($_SESSION['email'] == "admin@admin.com"){
            ?>
                  <li class="nav-item">
                   <a class="nav-link font-weight-bold" href="../views/add_product.php">Add Product</a>
                  </li>
            <?php
                }else{
            ?>
              <li class="nav-item">
              <a class="nav-link font-weight-bold" href="../views/cart.php">Show Cart</a>
               </li>
          
            <?php
          }
          ?>
        
        <li class="nav-item">
        <a class="nav-link font-weight-bold" href="#">Hello, <?php echo $_SESSION['name'] ?></a>
        </li>
         <li class="nav-item">
         <a class="nav-link font-weight-bold" href="../controllers/process_logout.php">Logout</a>
         </li>


         <!--  -->
          <?php
        }else{
          ?>
           <li class="nav-item">
        <a class="nav-link font-weight-bold" href="../views/login.php">Login</a>
         </li>
        <li class="nav-item">
        <a class="nav-link font-weight-bold" href="../views/register.php">Register</a>
         </li>
          <?php
        }
         ?>
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search">
      <button class="btn btn-warning my-2 my-sm-0" type="submit">Search</button>
    </form>
  <!--   <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form> -->
  </div>
</nav>