<!DOCTYPE html>
<html>
<head>
	<title>Accessories Store</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/pulse/bootstrap.css">
	
</head>
<body class="bg-warning">
	<?php require "navbar.php";
	get_content();
	require "footer.php";
	 ?>
</body>
</html>