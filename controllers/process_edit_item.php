<?php 
	require "connection.php";

	function validate_form(){
		$errors = 0;
		$file_types = ["jpg", "jpeg", "png", "gif", "bmp", "svg"];
		$file_ext = strtolower(pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION));

		//Validation Logic
		//We'll check if each of the fields in the form has a value. if not, it will increase the $error total and if the $error total > 0, it will return false.
		//we'll check if the file extension of the image is within the acceptable file extensions. if not,it will return false.
		if($_POST['name']=="" || ! isset($_POST['name'])){
			$errors++;
		};

		if($_POST['price']<=0 || !isset($_POST['price'])){
		$errors++;
		};

		if($_POST['description'] == "" || !isset($_POST['description'])){
			$errors++;
		};

		if($_POST['category_id'] == "" || !isset($_POST['category_id'])){
			$errors++;
		};

		if($_FILES['image']['name'] != ""){
			if(!in_array($file_ext, $file_types)){
			$errors++;
			
			}
		};

		if($errors>0){
			return false;
		}else{
			return true;
		}
	};

	if(validate_form()){
		//PROCESS OF SAVING AN ITEM
		//1. CAPTURE ALL DATA FROM FORM THROUGH $_POST OR $_FILES FOR IMAGE
		//2. MOVE UPLOADED IMAGE FILE TO THE ASSETS/IMAGES DIRECTORY
		//3. CREATE THE QUERY
		//4. USE MYSQLI_QUERY
		//5. go back to catalog if successful
		//6. if unsuccessful go back to add_item_form
		$id = $_POST['id'];
		$name = $_POST['name'];
		$price = $_POST['price'];
		$description = $_POST['description'];
		$category_id = $_POST['category_id'];

		$item_query = "SELECT image FROM items WHERE id = $id"; 

		$image_result = mysqli_fetch_assoc(mysqli_query($conn, $item_query));



		$image="";

// check
		if($_FILES['image']['name']==""){
			$image = $image_result['image'];
		}else{
			$destination = "../assets/images/";

		$file_name = $_FILES['image']['name'];

		move_uploaded_file($_FILES['image']['tmp_name'], $destination.$file_name);

		$image = $destination . $file_name;

		};


		// var_dump($name);
		// var_dump($price);
		// var_dump($description);
		// var_dump($image);
		// var_dump($category_id);
		// die();



		$destination = "../assets/images/";

		$file_name = $_FILES['image']['name'];

		move_uploaded_file($_FILES['image']['tmp_name'], $destination.$file_name);

		$image = $destination . $file_name;

		

		$update_item_query = "UPDATE items SET name = '$name', price = $price, description = '$description', image ='$image', category_id = $category_id WHERE id = $id";
		$result = mysqli_query($conn, $update_item_query);

		header("Location: ../views/catalog.php");
	}else{
		header("Location: " . $_SERVER['HTTP_REFERER']);
	};

 ?>